﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using GlobalizedValidationTest.Models;

namespace GlobalizedValidationTest.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {
            
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }

        public ActionResult Index()
        {
            return View(new TestViewModel { TestDouble = 123456.789, TestInt = 123456789});
        }
        [HttpPost]
        public ActionResult Index(TestViewModel model)
        {
            var test = model.TestDouble;
            return this.View();
        }
    }
}
