﻿using System;
using System.Threading;

namespace GlobalizedValidationTest.CustomBinders
{
    public class DecimalModelBinder : CultureSafeModelBinder
    {
        public override Func<string, object> Converter
        {
            get
            {
                return s => Convert.ToDecimal(s, Thread.CurrentThread.CurrentUICulture);
            }
        }
    }
}