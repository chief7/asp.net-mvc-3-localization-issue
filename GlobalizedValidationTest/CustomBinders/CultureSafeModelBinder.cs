﻿using System;
using System.Web.Mvc;

namespace GlobalizedValidationTest.CustomBinders
{
    public abstract class CultureSafeModelBinder : IModelBinder
    {
        public abstract Func<string, object> Converter { get; }

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            ValueProviderResult valueResult = bindingContext.ValueProvider
                .GetValue(bindingContext.ModelName);
            ModelState modelState = new ModelState { Value = valueResult };
            object actualValue = null;
            try
            {
                actualValue = Converter(valueResult.AttemptedValue);
            }
            catch (FormatException e)
            {
                modelState.Errors.Add(e);
            }
            
            if (bindingContext.ModelState.ContainsKey(bindingContext.ModelName))
                bindingContext.ModelState[bindingContext.ModelName] = modelState;
            else
                bindingContext.ModelState.Add(bindingContext.ModelName, modelState);

            return actualValue;
        }

    }
}