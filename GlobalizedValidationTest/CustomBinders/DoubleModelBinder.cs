﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

namespace GlobalizedValidationTest.CustomBinders
{
    public class DoubleModelBinder : CultureSafeModelBinder
    {
        public override Func<string, object> Converter
        {
            get
            {
                return s => Convert.ToDouble(s, Thread.CurrentThread.CurrentUICulture);
            }
        }
    }
}