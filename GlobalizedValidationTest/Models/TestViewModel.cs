﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GlobalizedValidationTest.Models
{
    public class TestViewModel
    {
        [Range(0,1000000)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:N2}")]
        public double TestDouble { get; set; }

        public int TestInt { get; set; }
    }
}